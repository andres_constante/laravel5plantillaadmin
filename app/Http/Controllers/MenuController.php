<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MenuModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;
class MenuController extends Controller
{
    var $configuraciongeneral = array ("Menú de Opciones", "menu", "index");
    var $escoja=array(null=>"Escoja opción...") ;
    var $objetos = '[ 
        {"Tipo":"text","Descripcion":"Nombre del Menú","Nombre":"menu","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }, 
        {"Tipo":"text","Descripcion":"Ruta / URL","Nombre":"ruta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"text","Descripcion":"Icono","Nombre":"iconfaw","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO" },
        {"Tipo":"select","Descripcion":"Nivel","Nombre":"nivel","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"select-ajax","Descripcion":"ID Padre","Nombre":"idmain","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO" },
        {"Tipo":"text","Descripcion":"Orden","Nombre":"orden","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"text","Descripcion":"Adicional","Nombre":"adicional","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO"  },
        {"Tipo":"select","Descripcion":"Visible","Nombre":"visible","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
                  ]'; 
    /*var $validarjs =array(
            "nombre"=>"nombre: {
                            required: true
                        }",
            "descripcion"=>"descripcion: {
                            required: true
                        }",
            "valor"=>"valor: {
                            required: true
                        }",
            "requerido"=>"requerido: {
                            required: true
                        }",
            "orden"=>"orden: {
                            required: true
                        }",
            "visiblecampo"=>"visiblecampo: {
                            required: true
                        }",
            "valor_predefinido"=>"valor_predefinido: {
                            required: true
                        }",
            "tiposolicitudes"=>"'tiposolicitudes[]': {
                            required: true
                        }"
        );*/
    public function __construct() {
        $this->middleware('auth');
    } 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$objetos=json_decode($this->objetos);
        //$objetos[6]->Valor= $this->escoja + (array)$tiposolicitudes;
        //$objetos[6]->Nombre=$objetos[6]->Nombre."[]";
        $main=MenuModel::orderby("orden")->lists("menu","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[4]->Valor= $this->escoja + $main;
        $objetos[3]->Valor=array(1=>"1",2=>"2");
        $objetos[7]->Valor=array("SI"=>"SI","NO"=>"NO");
        //show($objetos);        
        $objetos=array_values($objetos);        
        $tabla= MenuModel::join("menu as b","menu.idmain","=","b.id") 
                    ->select("menu.*" ,"b.menu as idmainlista")
                    ->orderby("menu.orden","desc")
                    ->get(); 
        $tabla=MenuModel::orderby("orden","desc")->get();//->paginate(500);
        return view('permisosmenu.menuindex',[
                "objetos"=>$objetos,
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral,
                "delete"=>"si",
                "urlmenu"=>"permisostipousu"
                ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosenvio= Input::get("datosenvio");
        $id = trim($datosenvio[0]);

         //return $datosenvio;

        // Validacion
         $objetos=json_decode($this->objetos);
         $objetos=array_values($objetos);
         //return $objetos;
        foreach($objetos as $keycam => $valuecam)
        {                
          if ($datosenvio[$keycam + 1] == "-" || $datosenvio[$keycam + 1] == "" )
           {
               if ($valuecam->Requerido == "SI")
                 {
                   return "Por Favor Ingrese correctamente en el campo " . $valuecam->Nombre;
                 }                
           }; 
           if($valuecam->Nombre=="menu" && $id=="-")
            {
                $verificarexisteid = MenuModel::where("menu",trim($datosenvio[$keycam + 1]))->first();
                if($verificarexisteid)
                    return "El menú '".trim($datosenvio[$keycam + 1])."' ya existe.";
            }       
        } 
        //

         $verificarexisteid = MenuModel::find($id);


         if (!$verificarexisteid)
         {
            $guardar= new MenuModel;

         }else {
            $guardar= MenuModel::find($id);
         }   

         foreach($objetos as $keycam => $valuecam)
                 {
                    $dato=trim($datosenvio[$keycam + 1]);
                    if($dato=="-")
                        $dato="";
                    $nombre=$valuecam->Nombre;
                    $guardar->$nombre= $dato;
                 } 

                 $guardar->save();
                 
            return $guardar->id;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
