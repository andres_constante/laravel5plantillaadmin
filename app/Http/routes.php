<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('layout', function () {
    return view('home');
});
Route::get('/', ['middleware' => 'auth', function() {
    // Only authenticated users may enter...
    return view('home');
}]);
//Route::get('home', 'HomeController@index');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);


// Password reset link request routes...
Route::get('password/email', ['as' => 'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as' => 'password/postEmail', 'uses' => 'Auth\PasswordController@postEmail']);

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', ['as' => 'password/postReset', 'uses' =>  'Auth\PasswordController@postReset']);

//Menu
Route::resource('menu','MenuController');
//Usuarios
Route::resource('usuarios','UsuariosController');
//Editar Datos del Usuario
Route::resource('cambiardatos','CambiaDatosController');
//Tipo de Usuario
Route::resource('tipousuario','TipousuarioController');
//Perfil de Usuario
Route::resource('perfil','PerfilController');
//Permisos de Usuario
//Route::resource('permisosusuario','PermisosUsuariosController');
//Route::resource('permisosasignar','PermisosAsignarController');
//Permisos Perfiles
//Route::resource('perfilpermisos','PerfilPermisosController');
Route::resource('perfilpermisosasignar', 'AsignarPermisosPerfilController');
//Auditoría
Route::resource('auditoria','AuditoriaController');
//Variables de Configuracion
Route::resource('config','SysConfigController');
//
Route::get("permisostipousu","AsignarPermisosController@permisostipousu");
Route::resource("asignarpermisos","AsignarPermisosController");
//Route::get('');
//Pruebas
Route::get('prueba',function(){
	echo fechas(3,'2015-02-01 13:25:25')."<br>";
	echo validarCI('1309366514')."<br>";
	echo validarCI('1309366515')."<br>";
        echo limitarPalabras("Hola mundo como estas", 2);        
});
