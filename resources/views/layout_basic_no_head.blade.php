<?php
//use Request;
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>@yield('titulo')</title>
   
    {{ HTML::style('css/bootstrap.min.css') }}       
    {{ HTML::style('font-awesome/css/font-awesome.css') }}
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}
 
   <!-- Data Tables -->
    {{ HTML::style('css/plugins/dataTables/dataTables.bootstrap.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.responsive.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.tableTools.min.css') }}
       
<!-- Mensaje -->

{{ HTML::style('css/plugins/toastr/toastr.min.css') }}

    @yield('estilos')
<!-- Mainly scripts -->
{{ HTML::script('js/jquery-2.1.1.js') }} 
{{ HTML::script('js/bootstrap.min.js') }} 
{{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }} 
{{ HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }} 


<!-- Custom and plugin javascript -->
{{ HTML::script('js/inspinia.js') }} 
{{ HTML::script('js/plugins/pace/pace.min.js') }} 

 <!-- Data Tables -->
 {{ HTML::script('js/plugins/dataTables/jquery.dataTables.js') }}     
    {{ HTML::script("js/plugins/dataTables/dataTables.bootstrap.js") }} 
    {{ HTML::script("js/plugins/dataTables/dataTables.responsive.js") }} 
    {{ HTML::script("js/plugins/dataTables/dataTables.tableTools.min.js") }} 

 <!-- Mensaje -->
 {{ HTML::script('js/plugins/toastr/toastr.min.js') }}     
 
      <!-- CSS Notificacion -->
    {{ HTML::style('ventanas-modales/ventanas-modales.css') }}
    <!-- CSS Notificacion -->
    {{ HTML::script('ventanas-modales/ventanas-modales.js') }}  
    <!-- Graficos estadissticos -->
{{ HTML::style('css/plugins/morris/morris-0.4.3.min.css') }}
{{ HTML::script('js/plugins/morris/raphael-2.1.0.min.js') }}
{{ HTML::script('js/plugins/morris/morris.js') }}
<!-- Chosen -->
    {{ HTML::style('chosen/docsupport/prism.css') }}
    {{ HTML::style('chosen/chosen.css') }}
    
    {{ HTML::script('chosen/chosen.jquery.js') }}
    {{ HTML::script('chosen/docsupport/prism.js') }}
<!-- ColorBox -->
    {{ HTML::style('colorbox/colorbox.css') }}    
    
    {{ HTML::script('colorbox/jquery.colorbox.js') }}    
@yield('scripts')
 <script>
 function cargaatender()
 {
	 
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificatodos") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txttodos").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},				
			success: function(datos){
				$("#txttodos").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txttodos").html("<div class='alert alert-danger'>No existe URL</div>");
		    }	
			}	  
    }); 	 
 }
 function NotificacionTotal()
 {
	    $.ajax({
			type: "GET",
			url: '{{ URL::to("getnotificacuenta") }}',
			data: '',
			error: function(objeto, quepaso, otroobj){
				$("#txtcontar").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
			},				
			success: function(datos){
				$("#txtcontar").html(datos);
		  },
  			statusCode: {
			    404: function() {
				$("#txtcontar").html("<div class='alert alert-danger'>No existe URL</div>");
		    }	
			}	  
    }); 
 	 //setTimeout(NotificacionTotal, 5000);
 }
 //setTimeout(NotificacionTotal, 1000);
 

 </script>
</head>
<body class="top-navigation">
<div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
       
    <!-- Cuerpo -->    
    <div class="row wrapper border-bottom white-bg page-heading">
                    @yield('contenido')
            </div>
            
        <div class="footer">
            <div class="pull-right">
                <!--10GB of <strong>250GB</strong> Free. -->
                {{ isset(Auth::user()->name) ? Auth::user()->name : '' }}                
            </div>
            <div>               
                {{ trans('html.main.copyright') }}
            </div>
        </div>

        </div>
        </div>

</body>

</html>
