<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do 
    |
    */
    'main' => [
        'sistema' => 'Sistema Principal',
        'sistemasub' => 'Sistema Principal Gobierno Autónomo Descentralizado del Cantón Manta',
        'copyright' => 'Copyright TIC GAD Manta &copy; 2014-2019 - Desarrollado por: Andrés Constante'
    ],
];
