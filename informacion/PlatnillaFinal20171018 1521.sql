-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.14


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema plantilladmin
--

CREATE DATABASE IF NOT EXISTS plantilladmin;
USE plantilladmin;

--
-- Definition of table `ad_auditoria`
--

DROP TABLE IF EXISTS `ad_auditoria`;
CREATE TABLE `ad_auditoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) NOT NULL,
  `urlmenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `accion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nompc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_auditoria`
--

/*!40000 ALTER TABLE `ad_auditoria` DISABLE KEYS */;
INSERT INTO `ad_auditoria` (`id`,`idusuario`,`urlmenu`,`accion`,`ip`,`nompc`,`created_at`,`updated_at`) VALUES 
 (1,3,'/plantilladmin/public/config','Acceso no permitido 403','::1','localhost','2017-10-17 13:06:09','2017-10-17 13:06:09'),
 (2,3,'/plantilladmin/public/perfil','Registro Variable de Configuración - ID: 0-','::1','localhost','2017-10-17 17:51:06','2017-10-17 17:51:06'),
 (3,3,'/plantilladmin/public/usuarios/4','Edición Variable de Configuración - ID: 4-','::1','localhost','2017-10-17 18:42:19','2017-10-17 18:42:19'),
 (4,3,'/plantilladmin/public/usuarios/4','Edición Variable de Configuración - ID: 4-','::1','localhost','2017-10-17 18:44:25','2017-10-17 18:44:25'),
 (5,3,'/laravel5plantillaadmin/public/perfilpermisosasignar','Asignación de Permisos por Perfil - ID: 2','::1','localhost','2017-10-18 06:40:23','2017-10-18 06:40:23'),
 (6,3,'/laravel5plantillaadmin/public/perfilpermisosasignar','Asignación de Permisos a Perfil. ID Perfil 2','::1','localhost','2017-10-18 06:49:08','2017-10-18 06:49:08'),
 (7,3,'/laravel5plantillaadmin/public/perfilpermisosasignar','Asignación de Permisos por Perfil - ID: 2','::1','localhost','2017-10-18 06:49:08','2017-10-18 06:49:08'),
 (8,3,'/plantilladmin/public/menu','Acceso no permitido 403','::1','localhost','2017-10-18 10:04:57','2017-10-18 10:04:57'),
 (9,3,'/plantilladmin/public/menu','Acceso no permitido 403','194.170.13.12','194.170.13.35','2017-10-18 10:10:28','2017-10-18 10:10:28');
/*!40000 ALTER TABLE `ad_auditoria` ENABLE KEYS */;


--
-- Definition of table `ad_menuusuario`
--

DROP TABLE IF EXISTS `ad_menuusuario`;
CREATE TABLE `ad_menuusuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) unsigned NOT NULL,
  `idmenu` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ad_menuusuario_menu1_idx` (`idmenu`),
  KEY `fk_ad_menuusuario_users1_idx` (`idusuario`),
  CONSTRAINT `fk_ad_menuusuario_menu1` FOREIGN KEY (`idmenu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ad_menuusuario_users1` FOREIGN KEY (`idusuario`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_menuusuario`
--

/*!40000 ALTER TABLE `ad_menuusuario` DISABLE KEYS */;
INSERT INTO `ad_menuusuario` (`id`,`idusuario`,`idmenu`,`created_at`,`updated_at`) VALUES 
 (1,4,2,NULL,NULL),
 (2,4,1,NULL,NULL);
/*!40000 ALTER TABLE `ad_menuusuario` ENABLE KEYS */;


--
-- Definition of table `ad_perfil`
--

DROP TABLE IF EXISTS `ad_perfil`;
CREATE TABLE `ad_perfil` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `perfil` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACT',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_perfil`
--

/*!40000 ALTER TABLE `ad_perfil` DISABLE KEYS */;
INSERT INTO `ad_perfil` (`id`,`perfil`,`estado`,`created_at`,`updated_at`) VALUES 
 (1,'GENERAL','ACT','2017-10-17 17:48:12','2017-10-17 17:48:12'),
 (2,'CONSULTAS','ACT','2017-10-17 17:51:06','2017-10-17 17:51:06');
/*!40000 ALTER TABLE `ad_perfil` ENABLE KEYS */;


--
-- Definition of table `ad_perfilpermisos`
--

DROP TABLE IF EXISTS `ad_perfilpermisos`;
CREATE TABLE `ad_perfilpermisos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idperfil` int(11) unsigned NOT NULL,
  `idmenu` int(11) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ad_perfilpermisos_ad_perfil1_idx` (`idperfil`),
  KEY `fk_ad_perfilpermisos_menu1_idx` (`idmenu`),
  CONSTRAINT `fk_ad_perfilpermisos_ad_perfil` FOREIGN KEY (`idperfil`) REFERENCES `ad_perfil` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ad_perfilpermisos_menu1` FOREIGN KEY (`idmenu`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_perfilpermisos`
--

/*!40000 ALTER TABLE `ad_perfilpermisos` DISABLE KEYS */;
INSERT INTO `ad_perfilpermisos` (`id`,`idperfil`,`idmenu`,`created_at`,`updated_at`) VALUES 
 (6,2,2,'2017-10-18 06:49:08','2017-10-18 06:49:08'),
 (7,2,1,'2017-10-18 06:49:08','2017-10-18 06:49:08');
/*!40000 ALTER TABLE `ad_perfilpermisos` ENABLE KEYS */;


--
-- Definition of table `ad_tipousuario`
--

DROP TABLE IF EXISTS `ad_tipousuario`;
CREATE TABLE `ad_tipousuario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'ACT',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ad_tipousuario_tipo_usuario_unique` (`tipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ad_tipousuario`
--

/*!40000 ALTER TABLE `ad_tipousuario` DISABLE KEYS */;
INSERT INTO `ad_tipousuario` (`id`,`tipo_usuario`,`estado`,`created_at`,`updated_at`) VALUES 
 (1,'ADMINISTRADOR','ACT',NULL,NULL),
 (2,'USUARIO','ACT','2016-05-23 12:02:19','2017-10-17 17:56:46');
/*!40000 ALTER TABLE `ad_tipousuario` ENABLE KEYS */;


--
-- Definition of table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `iconfaw` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `nivel` int(11) NOT NULL,
  `idmain` int(11) NOT NULL,
  `orden` decimal(10,2) NOT NULL,
  `adicional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `visible` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`,`menu`,`ruta`,`iconfaw`,`nivel`,`idmain`,`orden`,`adicional`,`visible`,`created_at`,`updated_at`) VALUES 
 (1,'Administración','javascript:','fa fa-pencil-square-o',1,0,'1.00','','SI','2017-10-17 19:19:43','2017-10-17 19:19:43'),
 (2,'Menú de Opciones','menu','fa fa-pencil-square-o',2,1,'2.00','','SI','2017-10-17 19:54:30','2017-10-17 19:54:30'),
 (3,'Auditoría','auditoria','',2,1,'3.00','',NULL,'2016-05-23 17:05:34','2016-05-23 21:17:02'),
 (4,'Usuarios del Sistema','#','fa fa-user',1,4,'4.00','',NULL,'2016-05-23 17:06:10','2016-05-23 17:06:17'),
 (5,'Usuarios','usuarios','',2,4,'5.00','',NULL,'2016-05-23 17:07:20','2016-05-23 17:07:20'),
 (6,'Tipos de Usuarios','tipousuario','',2,4,'6.00','',NULL,'2016-05-23 17:07:43','2016-05-23 17:08:21'),
 (7,'Permisos de Usuarios','asignarpermisos','',2,4,'7.00','','SI','2016-05-23 17:08:46','2017-10-18 06:52:27'),
 (8,'Perfiles de Usuario','#','fa fa-user-secret',1,8,'8.00','',NULL,'2016-05-23 17:09:43','2016-05-23 17:09:52'),
 (9,'Perfiles','perfil','',2,8,'9.00','',NULL,'2016-05-23 17:10:16','2016-05-23 17:10:16'),
 (10,'Permiso por Perfiles','perfilpermisosasignar','',2,8,'10.00','','SI','2016-05-23 17:10:42','2017-10-18 06:53:01'),
 (11,'Variables de Configuración','config','',2,1,'4.10','',NULL,'2016-05-23 22:14:31','2016-05-23 22:14:31');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


--
-- Definition of table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


--
-- Definition of table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;


--
-- Definition of table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sessions`
--

/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;


--
-- Definition of table `sysconfig`
--

DROP TABLE IF EXISTS `sysconfig`;
CREATE TABLE `sysconfig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `valorsub` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sysconfig_campo_unique` (`campo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sysconfig`
--

/*!40000 ALTER TABLE `sysconfig` DISABLE KEYS */;
/*!40000 ALTER TABLE `sysconfig` ENABLE KEYS */;


--
-- Definition of table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cedula` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0000000000',
  `fecha_nacimiento` date NOT NULL,
  `id_tipo_usuario` int(11) unsigned NOT NULL DEFAULT '1',
  `id_perfil` int(11) DEFAULT '0',
  `nuevo` int(11) NOT NULL DEFAULT '1',
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACT',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_cedula_unique` (`cedula`),
  KEY `fk_users_ad_tipousuario1_idx` (`id_tipo_usuario`),
  CONSTRAINT `fk_users_ad_tipousuario1` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `ad_tipousuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`,`cedula`,`telefono`,`fecha_nacimiento`,`id_tipo_usuario`,`id_perfil`,`nuevo`,`estado`,`deleted_at`) VALUES 
 (1,'Administrador','admin@admin.com','$2y$10$wWhm4D6/Qa.G6AouNG0EfuWCCkJQCHP1avM4bMJWUer87l8nJCsJ.','lDWniJa7Hrklvj6tIf060KV9MPgQtj4bg7wTH6iy0Z6Wz91cgnz2PtYgGi4w',NULL,'2017-10-18 10:11:33','admin','0000000000','2016-05-25',1,0,1,'ACT',NULL),
 (3,'Andres Constante','aconstante@manta.gob.ec','$2y$10$xoPe8kevW6dOS/kffod1GOXC2MX6x5GWQ9zyy6vWhoSVy8gXKe9Qm','PL1S2JpczOoHcsHshgqGG6RIJnVxxksONWjjwpOb0tj2pICg7qr1wJkmWbAX','2016-05-20 19:59:40','2017-10-18 14:18:37','1309366514','0994978134','2016-05-25',1,1,0,'ACT','2016-05-26 14:31:57'),
 (4,'Virginia Intriago Cedeño','Vicky-cg@hotmail.com','$2y$10$BDyCi3q2MTJzjFQLzsxihuc5iERVhC35P../HLUd.gC0nQRrUKXSG',NULL,'2017-10-17 18:37:17','2017-10-17 18:45:26','1314494905','0981500524','2017-10-17',2,2,1,'ACT',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
